﻿

using System;
using System.Linq;

namespace TrojmiastoOgloszenia
{
    public class Program 
    {
        static void Main(string[] args)
        {
            MotoWebObjects page = new MotoWebObjects();
            double averageYear = page.GetCarDetails(3).Average();

            Console.WriteLine("Average of year is equal to : " + averageYear);

            double averageMileage = page.GetCarDetails(5).Average();

            Console.WriteLine("Average of mileage is equal to : " + averageMileage);

            
        }
    }
}
