﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TrojmiastoOgloszenia
{
    class MotoWebObjects
    {
        IWebDriver driver = new ChromeDriver();

        public MotoWebObjects()
        {
            var url = "https://ogloszenia.trojmiasto.pl/motoryzacja-sprzedam/";
            driver.Navigate().GoToUrl(url);

            driver.FindElement(By.CssSelector("input.oglSearchbar__input.input.input--has--clear.ui-autocomplete-input")).SendKeys("Opel");          
            driver.FindElement(By.CssSelector("button.oglSearchbar__btn.btn.btn--orange")).Click();
        }


        // number - 0 : model | 1 : location | 3 : year | 7 : capacity | 5 : mileage 
        public List<int> GetCarDetails(int number)
        {
            var detailsList = new List<int>();
            var cars = driver.FindElements(By.CssSelector("div.list__item__content"));

            foreach(var details in cars)
            {
                var detailsText = details.Text;
                string carsDetails = detailsText.ToString();
                string[] carsDetailsList = carsDetails.Split('\n');

                string carText = carsDetailsList[number].Remove(carsDetailsList[number].Length-4);
                int value = Int32.Parse(carText);
             
                detailsList.Add(value);
            }

            return detailsList;
     
        }

    }
       
        
}
